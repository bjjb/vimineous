# vimineous

> *və̇ˈminēəs*: of or producing long slender twigs or shoots

This is a very opinionated Vim plugin which adds configuration for various
programming languages using only the support built into Vim and the most basic
tooling that can be expected with each language. The name reflects that - it's
for Vim (obviously), and it adds minimal, slender shoots to the editor to help
you get your job done.

It was inspired by [some][1] [excellent][2] [talks][3] I caught on YouTube by
[leeren](https://www.youtube.com/@leeren_), where he explains in detail how Vim
can already do 90% of what a bloated IDE can do with its built-in capabilities.

There's another (much better!) plugin available with a similar function called
[vim-polyglot](https://github.com/sheerun/vim-polyglot), which I would
definitely recommend instead of this.

The motivation here is to drastically reduce the amount of plugins I need to
load, and to ensure a consistent workflow when hopping between projects in Go,
Ruby, TypeScript, Crystal, Rust, etc. It assumes Vim 9+ (or maybe NeoVim), and
uses `include-search`, `errorformat`, etc, alongside the stock editor's
built-in language support. It might attempt to do some omni-completion in the
future, but that's not a priority.

It's very much a work-in-progress, absolutely tailored to my own personal
preference, and will probably annoy you. But if you wanna give it a whirl in a
clean Vim install, just shallow-clone it into
`~/.vim/pack/bjjb/start/vimineous` and open a file in one of the supported
languages.

## Languages

Follow the links for details of the supported language features.

- [ ] [C][c]
- [ ] [Clojure][clj]
- [ ] [Crystal][cr]
- [ ] [Elixir][ex]
- [ ] [Go][go]
- [ ] [Groovy][groovy]
- [ ] [Java][java]
- [ ] [Javascript][js]
- [ ] [Python][py]
- [ ] [Ruby][rb]
- [ ] [Scala][scala]
- [ ] [Terraform][tf]
- [ ] [TypeScript][ts]

[1]: https://www.youtube.com/watch?v=E-ZbrtoSuzw
[2]: https://www.youtube.com/watch?v=JFr28K65-5E
[3]: https://www.youtube.com/watch?v=Gs1VDYnS-Ac
[c]: https://gitlab.com/bjjb/vimineous/-/wikis/C
[clj]: https://gitlab.com/bjjb/vimineous/-/wikis/Clojure
[cr]: https://gitlab.com/bjjb/vimineous/-/wikis/Crystal
[ex]: https://gitlab.com/bjjb/vimineous/-/wikis/Elixir
[go]: https://gitlab.com/bjjb/vimineous/-/wikis/Go
[groovy]: https://gitlab.com/bjjb/vimineous/-/wikis/Groovy
[java]: https://gitlab.com/bjjb/vimineous/-/wikis/Java
[js]: https://gitlab.com/bjjb/vimineous/-/wikis/Javascript
[py]: https://gitlab.com/bjjb/vimineous/-/wikis/Python
[rb]: https://gitlab.com/bjjb/vimineous/-/wikis/Ruby
[scala]: https://gitlab.com/bjjb/vimineous/-/wikis/Scala
[tf]: https://gitlab.com/bjjb/vimineous/-/wikis/Terraform
[ts]: https://gitlab.com/bjjb/vimineous/-/wikis/TypeScript
